using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace MyVod.Infrastructure.Movies;

public class MoviesContext : DbContext
{
    
    public MoviesContext(DbContextOptions<MoviesContext> options) : base(options)
    {
    }
        
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    { 
        optionsBuilder.UseLoggerFactory(MyLoggerFactory);
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
    }

    public static string Schema = "movies";
        
    static readonly ILoggerFactory MyLoggerFactory = LoggerFactory.Create(builder => { builder.AddConsole(); });
}