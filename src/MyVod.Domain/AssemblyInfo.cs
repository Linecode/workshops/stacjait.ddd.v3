using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("MyVod.DomainTests")]
[assembly: InternalsVisibleTo("MyVod.Infrastructure")]
[assembly: InternalsVisibleTo("MyVod.IntegrationTests")]